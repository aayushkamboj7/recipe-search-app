import React, { useState } from 'react';
import './App.css';

function App() {
  let [recipeData, updateData] = useState('');

  function handleSubmit(event) {
    event.preventDefault();
    fetch(
      `https://api.spoonacular.com/recipes/complexSearch?apiKey=59352e4d1eab489ca6c11b8f67186620&query=${event.target.dish.value}`
    )
      .then((res) => res.json())
      .then((data) => updateData(data.results));
  }

  if (!recipeData) {
    return (
      <div className="recipe-data">
        <h1>Search Food Recipes</h1>
        <form onSubmit={handleSubmit}>
          <input
            type="text"
            placeholder="Enter the name of the dish"
            name="dish"
          />
          <input type="submit" value="Search" />
        </form>
      </div>
    );
  } else {
    return (
      <div className="recipes">
        {recipeData.map((recipe) => {
          return (
            <div className="singleRecipe" key={recipe.id}>
              <h1>
                <a href="/singleRecipe">{recipe.title}</a>
              </h1>
              <img src={recipe.image} alt={recipe.title} />
            </div>
          );
        })}
      </div>
    );
  }
}

export default App;
